'''
Created on Jul 4, 2017

@author: paleksic@ztech.io
'''
import sys 
import utilitiesClass

busName = sys.argv[0]
jmsASName = sys.argv[1]
jmsASJndiName = sys.argv[2]
jmsQueueJndiName = sys.argv[3]
authAlias = sys.argv[4]
destinationType = "Queue"
clusterName = sys.argv[5]

utilities = utilitiesClass.Helpers()
alias = utilities.findAliasName(authAlias)
    
args = ["-busName", busName,
         "-name", jmsASName, 
         "-jndiName", jmsASJndiName, 
         "-destinationJndiName",  jmsQueueJndiName, 
         "-destinationType" , destinationType, 
         "-authenticationAlias", alias]

try:
    print "Creating activation specs"
    scope = AdminConfig.getid("/ServerCluster:"+clusterName)
    AdminTask.createSIBJMSActivationSpec(scope, args)
except:
    print "Failed creating Activation Specs"
    AdminConfig.reset()
    print "Reseting environment"
else:
    AdminConfig.save()
    print "Saving configuration"