'''
Created on Jul 4, 2017

@author: paleksic@ztech.io
'''
import os
class Helpers:
    
    # return JAAS Auth alias name
    def findAliasName(self, name):   
        # fetch all available aliases
        authAliases = AdminConfig.list("JAASAuthData").split(os.linesep)
        # loop
        for alias in authAliases:
            # look for JAAS name attribute 
            aliasName = AdminConfig.showAttribute(alias, 'alias')
            # compare two strings and return full name if available
            if(aliasName.find(name) >= 0):
                return aliasName
    
    # if directory is available delete it
    def isDirectory(self, path):
        if(os.path.exists(path)):
            print "Directory %s exists . Deleting it..." % (path)
            os.remove(path)
            print "Directory deleted"
        else:
            print "Creating directory %s" % (path)
            os.mkdir(path)