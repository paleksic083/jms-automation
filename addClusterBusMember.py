'''
Created on Jul 3, 2017

@author: paleksic@ztech.io
'''
import sys
import utilitiesClass

bus = sys.argv[0]
cluster = sys.argv[1]
fileStoreDirectory = sys.argv[2]
logDirectory = sys.argv[3]
policy = sys.argv[4]

# messages store and log directory
utilities = utilitiesClass.Helpers()
utilities.isDirectory(fileStoreDirectory+"PermanentStore")
utilities.isDirectory(logDirectory+"Log")
utilities.isDirectory(fileStoreDirectory+"TemporaryStore")

# options for sibus members    
args = '[ -bus ' + bus + \
        ' -cluster ' + cluster + \
        ' -fileStore ' + \
        ' -permanentStoreDirectory ' + fileStoreDirectory + \
        ' -logDirectory ' + logDirectory + \
        ' -temporaryStoreDirectory ' + fileStoreDirectory + \
        ' -enableAssistance true' + \
        ' -policyName ' + policy + \
        ' ]'

try: 
    print "Adding cluster to Bus members"       
    AdminTask.addSIBusMember(args)
except:
    print "Error while adding bus members"
    AdminConfig.reset()
    print "Reseting environment"
else:
    AdminConfig.save()
    print 'Configuration saved'