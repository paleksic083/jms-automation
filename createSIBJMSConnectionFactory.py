'''
Created on Jul 4, 2017

@author: paleksic@ztrech.io
'''
import sys
import utilitiesClass

#  args from properties file
busName = sys.argv[0]
jmsCFJndiName = sys.argv[1]
jmsCFName = sys.argv[2]
clusterName = sys.argv[4]
scope = AdminConfig.getid("/ServerCluster:"+clusterName)
aliasUserName = sys.argv[3]

utilities = utilitiesClass.Helpers()
foundAlias = utilities.findAliasName(aliasUserName)

methodArgs =  ["-name", jmsCFName,
                "-busName", busName,
                "-jndiName", jmsCFJndiName,
                 "-containerAuthAlias", foundAlias]

try:
    AdminTask.createSIBJMSConnectionFactory(scope, methodArgs)
except:
    print "Error creating connection factory\nReseting environment"
    AdminConfig.reset()
else:
    print "Connection factory created\nSaving configuration"
    AdminConfig.save()