'''
Created on Jul 4, 2017

@author: paleksic@ztech.io
'''
# Coonsumer App
import sys

earLocation = sys.argv[0]
cluster = sys.argv[1]

try:
    print "Installing consumer app"
    AdminApp.install(earLocation, ["-cluster", cluster])
except:
    print "Consumer app not installed\n Resetting environment"
    AdminApp.reset()
else:
    print "Saving configuration"
    AdminConfig.save()