'''
Created on Jul 3, 2017

@author: paleksic@ztech.io
'''
import sys

# fetch args from ANT properties file
busUser = sys.argv[0]
password = sys.argv[1]
firstName = sys.argv[2]
lastName = sys.argv[3]

execfile("utilitiesClass.py")
utilities = utilitiesClass()

try:
    print "Creating user %s" % (busUser)
    AdminTask.createUser(["-uid", busUser,
                          "-password", password,
                          "-cn", firstName,
                          "-sn", lastName,
                          "-confirmPassword", password])
except:
    print "Error while creating user %s" % (busUser)
    print "Reseting environment"
    AdminConfig.reset()
else:
    print "Saving configuration"
    AdminConfig.save()

try:
    print "Creating alias %s for user %" %(busUser, busUser)
    AdminTask.createAuthDataEntry(["-alias", busUser,
                                   "-password", password,
                                   "-user", busUser])
except:
    print "Error while creating alias %s" % (busUser)
    print "Reseting environment"
    AdminConfig.reset()
else:
    print "Saving configuration"
    AdminConfig.save()