'''
Created on Jul 4, 2017
@author: paleksic@ztech.io
'''
import sys
import utilitiesClass

# properties  
earLocation = sys.argv[0]
jmsQueueJndiName = sys.argv[1]
jmsCFJndiName = sys.argv[2]
authAlias = sys.argv[3]
clusterName = sys.argv[4]

# instantiate utilities class 
utilities = utilitiesClass.Helpers()
foundAlias = utilities.findAliasName(authAlias)

# build options 
args_1 = ["-cluster", clusterName]
args_2 = ["-MapMessageDestinationRefToEJB", [[".*", ".*", ".*", ".*", jmsQueueJndiName]]]
args_3 = ["-MapResRefToEJB", [[ ".*", ".*", ".*",".*", ".*",  jmsCFJndiName,  "DefaultPrincipalMapping", [ foundAlias ]]]]

args = (args_1 + args_2 + args_3)

try:
    print "Instaling producer app"
    AdminApp.install(earLocation, args)
except:
    print "Application could not be installed"
    AdminConfig.reset()
    print "Reset environment"
else:
    print "Saving configuration"
    AdminConfig.save()