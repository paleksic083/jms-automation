'''
Created on Jul 3, 2017

@author: paleksic@ztech.io
'''
#import AdminTask
import sys
busName = sys.argv[0]
username = sys.argv[1]
try:
    print "Adding user to Bus Connector Role"
    AdminTask.addUserToBusConnectorRole(["-bus", busName, "-user", username])
except:
    print "Error. Reseting environment"
    AdminConfig.reset()
else:
    print "Saving Configuration"
    AdminConfig.save()
