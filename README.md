```
#!python

./ws_ant.sh -f build.xml
Buildfile: build.xml

info:
     [echo]
     [echo] 			Possible targets:
     [echo] 			    createUserAndAuthAlias
     [echo] 				createSIBus
     [echo] 				addUserToBusConnectorRole
     [echo] 				addClusterBusMember
     [echo] 				createSIBQueueDestination
     [echo] 				createSIBJMSConnectionFactory
     [echo] 				createSIBJMSQueue
     [echo] 				createSIBJMSActivationSpec
     [echo] 				installProducerApp
     [echo] 				installConsumerApp
     [echo]

BUILD SUCCESSFUL
```