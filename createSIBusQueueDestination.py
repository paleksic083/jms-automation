'''
Created on Jul 3, 2017

@author: paleksic
'''
import sys

bus = sys.argv[0]
sibDestinationName = sys.argv[1]
cluster = sys.argv[2]


args = '[ -bus ' + bus + \
        ' -name ' + sibDestinationName + \
        ' -type Queue ' + \
        ' -cluster ' + cluster + \
        ' ]'

try:
    print 'Creating SIBus queue destination "' + sibDestinationName + '" on bus "' + bus + '".'
    AdminTask.createSIBDestination(args)
except:
    print "Error while creating sibus destination"
    AdminConfig.reset()
    print "Reseting environment"
else:
    AdminConfig.save()
    print 'Configuration saved'