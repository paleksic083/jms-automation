'''
Created on Jul 3, 2017

@author: paleksic@ztech.io
'''
import sys
busName = sys.argv[0]
busSecurity = sys.argv[1]

args = ["-bus", busName, "-busSecurity", busSecurity ]
try:
    print "Creating SIBus" + busName + "with bus security set to" + busSecurity + '.'
    AdminTask.createSIBus(args)
except:
    print "Error creating sibus"
    AdminConfig.reset()
    print "Reseting Environment"
else:
    AdminConfig.save()
    print "Saving Configuration" 