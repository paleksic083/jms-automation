'''
Created on Jul 3, 2017

@author: paleksic
'''
import sys

bus = sys.argv[0]           
jmsQueueName = sys.argv[1]     
jmsQueueJndiName = sys.argv[2]
sibDestinationName = sys.argv[3]
cluster = sys.argv[4]

args = '[ -busName ' + bus + \
        ' -name ' + jmsQueueName + \
        ' -jndiName ' + jmsQueueJndiName + \
        ' -queueName ' + sibDestinationName + \
        ' ]'
        
try:
    print 'Creating JMS Queue "' + jmsQueueName + '" with JNDI name "' + jmsQueueJndiName + '" at scope "' + cluster + '".'
    scope = AdminConfig.getid("/ServerCluster:"+cluster)
    AdminTask.createSIBJMSQueue(scope, args)
except: 
    print 'Failed creating queue'
    AdminConfig.reset()
    print "Reseting environment"
else:
    AdminConfig.save()
    print 'Configuration saved'